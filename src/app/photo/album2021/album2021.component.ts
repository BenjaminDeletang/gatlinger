import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-album2021',
  templateUrl: './album2021.component.html',
  styleUrls: ['./album2021.component.scss']
})
export class Album2021Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openModal(imageLink) {
    document.getElementById('content').innerHTML = `<img alt="Modal image" src="${imageLink}" style="box-shadow: 0px 1px 11px rgba(0, 0, 0, 0.4);" />`;
    document.getElementById('modal').style.display = 'flex';
  }

  closeModal() {
    document.getElementById('content').innerText = '';
    document.getElementById('modal').style.display = 'none';
  }

}

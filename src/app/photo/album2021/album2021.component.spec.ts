import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Album2021Component } from './album2021.component';

describe('Album2021Component', () => {
  let component: Album2021Component;
  let fixture: ComponentFixture<Album2021Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Album2021Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Album2021Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RocketleagueComponent } from './rocketleague.component';

describe('RocketleagueComponent', () => {
  let component: RocketleagueComponent;
  let fixture: ComponentFixture<RocketleagueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RocketleagueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RocketleagueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RainbowsixsiegeComponent } from './rainbowsixsiege.component';

describe('RainbowsixsiegeComponent', () => {
  let component: RainbowsixsiegeComponent;
  let fixture: ComponentFixture<RainbowsixsiegeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RainbowsixsiegeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RainbowsixsiegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

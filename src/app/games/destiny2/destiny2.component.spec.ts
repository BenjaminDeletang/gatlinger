import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Destiny2Component } from './destiny2.component';

describe('Destiny2Component', () => {
  let component: Destiny2Component;
  let fixture: ComponentFixture<Destiny2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Destiny2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Destiny2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

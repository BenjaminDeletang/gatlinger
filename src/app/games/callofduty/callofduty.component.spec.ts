import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallofdutyComponent } from './callofduty.component';

describe('CallofdutyComponent', () => {
  let component: CallofdutyComponent;
  let fixture: ComponentFixture<CallofdutyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallofdutyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallofdutyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColdwarComponent } from './coldwar.component';

describe('ColdwarComponent', () => {
  let component: ColdwarComponent;
  let fixture: ComponentFixture<ColdwarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColdwarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColdwarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

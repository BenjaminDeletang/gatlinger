import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import {HomeComponent} from './home/home.component';
import {SocialComponent} from './social/social.component';
import {PhotoComponent} from './photo/photo.component';
import {GamesComponent} from './games/games.component';
import { MinecraftComponent } from './games/minecraft/minecraft.component';
import { CallofdutyComponent } from './games/callofduty/callofduty.component';
import { CsgoComponent } from './games/csgo/csgo.component';
import { Destiny2Component } from './games/destiny2/destiny2.component';
import { RainbowsixsiegeComponent } from './games/rainbowsixsiege/rainbowsixsiege.component';
import { RocketleagueComponent } from './games/rocketleague/rocketleague.component';
import { Gta5Component } from './games/gta5/gta5.component';
import { WarzoneComponent } from './games/warzone/warzone.component';
import { ColdwarComponent } from './games/coldwar/coldwar.component';
import {RulesComponent} from './rules/rules.component';
import {Album2021Component} from './photo/album2021/album2021.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  // redirect to contact page
  {
    path: 'contact',
    component: ContactComponent
  },
  // redirect to social page
  {
    path: 'social',
    component: SocialComponent,
  },
  // redirect to photo page
  {
    path: 'photos',
    component: PhotoComponent,
  },
  // redirect to photo page
  {
    path: 'photos/album2021',
    component: Album2021Component,
  },
  // redirect to game page
  {
    path: 'jeux',
    component: GamesComponent,
  },
  {
    path: 'jeux/minecraft',
    component: MinecraftComponent,
  },
  {
    path: 'jeux/csgo',
    component: CsgoComponent,
  },
  {
    path: 'jeux/callofduty',
    component: CallofdutyComponent,
  },
  {
    path: 'jeux/destiny2',
    component: Destiny2Component,
  },
  {
    path: 'jeux/rainbowsixsiege',
    component: RainbowsixsiegeComponent,
  },
  {
  path: 'jeux/rocketleague',
  component: RocketleagueComponent,
  },
  {
    path: 'jeux/gta5',
    component: Gta5Component,
  },
  {
    path: 'jeux/warzone',
    component: WarzoneComponent,
  },
  {
  path: 'jeux/coldwar',
    component: ColdwarComponent,
  },
  // redirect to rules page
  {
    path: 'reglement',
    component: RulesComponent,
  },
  // otherwise redirect to home
  {
    path: '**',
    redirectTo: ''
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})

export class AppRoutingModule {

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { SocialComponent } from './social/social.component';
import { PhotoComponent } from './photo/photo.component';
import { NgbCarouselModule } from "@ng-bootstrap/ng-bootstrap";
import { Album2021Component } from './photo/album2021/album2021.component';
import { GamesComponent } from './games/games.component';
import { FooterComponent } from './footer/footer.component';
import { MinecraftComponent } from './games/minecraft/minecraft.component';
import { RainbowsixsiegeComponent } from './games/rainbowsixsiege/rainbowsixsiege.component';
import { Destiny2Component } from './games/destiny2/destiny2.component';
import { CallofdutyComponent } from './games/callofduty/callofduty.component';
import { CsgoComponent } from './games/csgo/csgo.component';
import { RocketleagueComponent } from './games/rocketleague/rocketleague.component';
import { Gta5Component } from './games/gta5/gta5.component';
import { ColdwarComponent } from './games/coldwar/coldwar.component';
import { WarzoneComponent } from './games/warzone/warzone.component';
import { RulesComponent } from './rules/rules.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    SocialComponent,
    PhotoComponent,
    Album2021Component,
    GamesComponent,
    FooterComponent,
    MinecraftComponent,
    RainbowsixsiegeComponent,
    Destiny2Component,
    CallofdutyComponent,
    CsgoComponent,
    RocketleagueComponent,
    Gta5Component,
    ColdwarComponent,
    WarzoneComponent,
    RulesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
